# Kite Runner symbolism webpage

This is a webpage with Javascript animations designed as a presentation on the symbol of literacy in
Khaled Hosseini's novel *The Kite Runner*. It is a project for Ms. Costa's ENG4U in May 2019.

If the book-turning gif doesn't load or has its frames out of order, move the gif offscreen and refresh.
In fact, if anything wonky happens, refresh. If viewing this webpage from a `file:///` URL, the CORS
policy will block some of the effects from loading. In that case, run Chrome (or another Chromium-based
browser) with the `--allow-file-access-from-files` command-line argument, or do the equivalent with
whatever browser you are using.
